//  知识点
//  JSON.parse() 方法将数据转换为 JavaScript 对象
//  使用 decodeURI() 对一个编码后的 URI 进行解码
//  split分割字符串为数组  replace字符串替换
//  用正则表达式
const getParameters = URL => JSON.parse(`{"${decodeURI(URL.split("?")[1]).replace(/"/g, '\\"').replace(/&/g, '","').replace(/=/g, '":"')}"}`)
console.log(`{"${decodeURI("https://www.google.com.hk/search?q=js+md&newwindow=1".split("?")[1]).replace(/&/g, '","').replace(/=/g, '":"')}"}`);
console.log(`{"${decodeURI("https://www.google.com.hk/search?q=js+md&newwindow=1".split("?")[1]).replace(/"/g, '\\"')}"}`);
console.log(getParameters("https://www.google.com.hk/search?q=js+md&newwindow=1"));