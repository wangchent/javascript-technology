
// 剪贴板 Clipboard API 为 Navigator 接口添加了只读属性 clipboard，该属性返回一个可以读写剪切板内容的 Clipboard 对象。在 Web 应用中，剪切板 API 可用于实现剪切、复制、粘贴的功能。
// 只有在用户事先授予网站或应用对剪切板的访问许可之后，才能使用异步剪切板读写方法。许可操作必须通过取得权限 Permissions API (en-US) 的 "clipboard-read" 和/或 "clipboard-write" 项获得。
// write() writeText() read() readText()


// Clipboard.write()方法用于将任意数据写入剪贴板，可以是文本数据，也可以是二进制数据。该方法接受一个 ClipboardItem 实例作为参数，表示写入剪贴板的数据。
const copyText = async (text) => await navigator.clipboard.write(text)
// ClipboardItem()是浏览器原生提供的构造函数，用来生成ClipboardItem实例，它接受一个对象作为参数，该对象的键名是数据的 MIME 类型，键值就是数据本身。
const item = new ClipboardItem({
    'text/plain':  new Blob(['Cute sleeping kitten'], {type: 'text/plain'}),
  });
copyText([item])


// 注意：navigator.clipboard.readText() 获取剪贴板的文本内容。需要注意的是执行该方法必须在 localhost 或 https:// 下并且最好是在点击事件里，否则可能获取失败。该方法也存在兼容性。IE11都不支持，需到IE edge 79才支持,IE可使用window.clipboardData做兼容处理


async function myFunction() {
//   navigator.clipboard
// .readText()
// .then((v) => {
//   console.log("获取剪贴板成功：", v);
// })
// .catch((v) => {
//   console.log("获取剪贴板失败: ", v);
// });

    try {
      const clipboardItems = await navigator.clipboard.read();
      console.log(clipboardItems);
      for (const clipboardItem of clipboardItems) {
        for (const type of clipboardItem.types) {
          const blob = await clipboardItem.getType(type);
          console.log(blob);
        }
      }
    } catch (err) {
      console.error(err.name, err.message);
    }
    }

//  Clipboard.read()方法用于复制剪贴板里面的数据，可以是文本数据，也可以是二进制数据（比如图片）。该方法需要用户明确给予许可。
// 该方法返回一个 Promise 对象。一旦该对象的状态变为 resolved，就可以获得一个数组，每个数组成员都是 ClipboardItem 对象的实例。
// ClipboardItem 对象表示一个单独的剪贴项，每个剪贴项都拥有ClipboardItem.types属性和ClipboardItem.getType()方法。
// ClipboardItem.types属性返回一个数组，里面的成员是该剪贴项可用的 MIME 类型，比如某个剪贴项可以用 HTML 格式粘贴，也可以用纯文本格式粘贴，那么它就有两个 MIME 类型（text/html和text/plain）。
// ClipboardItem.getType(type)方法用于读取剪贴项的数据，返回一个 Promise 对象。该方法接受剪贴项的 MIME 类型作为参数，返回该类型的数据，该参数是必需的，否则会报错。


// 参考文章http://www.ruanyifeng.com/blog/2021/01/clipboard-api.html
