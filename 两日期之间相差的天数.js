
// getTime 方法的返回值一个数值，表示从1970年1月1日0时0分0秒（UTC，即协调世界时）距离该日期对象所代表时间的毫秒数。
// Math.abs()返回一个数的绝对值
// 86400000=24小时*60分*60秒+1000毫秒=1天
// Math.ceil() 方法可对一个数进行上舍入，返回值大于或等于给定的参数。
const dayDiff = (date1, date2) => Math.ceil(Math.abs(date1.getTime() - date2.getTime()) / 86400000);

console.log(dayDiff(new Date("2021-10-21"), new Date("2022-02-12")) );
console.log(Math.abs(new Date("2021-10-21").getTime()-new Date("2022-02-12").getTime())/86400000);