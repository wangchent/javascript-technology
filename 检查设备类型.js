// userAgent 属性是一个只读的字符串，声明了浏览器用于 HTTP 请求的用户代理头的值。
// test() 方法用于检测一个字符串是否匹配某个模式.如果字符串中有匹配的值返回 true ，否则返回 false。
// /i时正则表达式 忽略大小写

const judgeDeviceType = () => /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|OperaMini/i.test(navigator.userAgent) ? 'Mobile' : 'PC';
// console.log(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|OperaMini/i.test(navigator.userAgent) );
console.log(navigator.userAgent);
console.log(judgeDeviceType());  // PC | Mobile