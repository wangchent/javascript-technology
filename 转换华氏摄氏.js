// 将华氏温度转换为摄氏温度
const fahrenheitToCelsius = (fahrenheit) => (fahrenheit - 32) * 5/9;
console.log(fahrenheitToCelsius(50));

// 将摄氏温度转华氏温度 : 摄氏温度c ,将其转化为华氏温度f ,转换公式为：f=c*9/5+32.
const celsiusToFahrenheit = (celsius) => celsius * 9/5 + 32;
console.log(celsiusToFahrenheit(37));