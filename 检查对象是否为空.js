// 静态方法 Reflect.ownKeys() 返回一个由目标对象自身的属性键组成的数组
const isEmpty = obj => Reflect.ownKeys(obj).length === 0 && obj.constructor === Object;
isEmpty({}) // true
isEmpty({a:""}) //false

console.log(isEmpty({}));
console.log(isEmpty({a:""}));