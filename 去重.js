  // Set是es6新增的数据结构，似于数组，但它的一大特性就是所有元素都是唯一的，没有重复的值，我们一般称为集合。Set本身是一个构造函数，用来生成 Set 数据结构
  const uniqueArr = (arr)=>[...new Set(arr)]
  // 数组去重
  const a = [1,1,2,3,4,4,3,5]
  const a_Duplicates= uniqueArr(a)
  console.log(a_Duplicates)
  // 用于字符串去重
  const name = '王大大明明'
  const name_Duplicates= uniqueArr(name).join('')
  console.log(name_Duplicates);