// window.getSelection()
// 定义和用法：getSelection() 方法表示用户选择的文本范围或光标的当前位置。
const getSelectedText = () => window.getSelection().toString();
function myFunction() {
    console.log(getSelectedText());
    }
