// 生成随机数相信你能信手拈来，那随机生成十六进制，例如生成十六进制颜色值。
// num.toString(16)将以十六进制形式返回数字的字符串表示形式。
// JavaScript中的padEnd()方法用于用另一个字符串填充一个字符串，直到达到给定的长度。从字符串的右端开始应用填充。
const randomHexColor = () => `#${Math.floor(Math.random() * 0xffffff).toString(16).padEnd(6, "0")}`
console.log(randomHexColor());