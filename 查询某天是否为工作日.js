// getDay() 方法可返回表示星期的某一天的数字。  返回的是0-6的数字,0 表示星期天
// getMonth(获取当前月份(0-11,0代表1月) 
const isWeekday = (date) => date.getDay() % 6 !== 0;
console.log(new Date() );
console.log(isWeekday(new Date(2023, 8, 10)))