// <<、>>和>>> 移位运算符
const rgbToHex = (r, g, b) =>   "#" + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);
console.log(rgbToHex(255, 255, 255));
// console.log(((1 << 24) + (255 << 16) + (255 << 8) + 255).toString(16));